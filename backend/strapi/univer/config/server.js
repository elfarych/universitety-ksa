const { APP_PORT, APP_IP, APP_PATH } = process.env;

module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port: env.int("PORT", 1337),
});
