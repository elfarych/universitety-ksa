'use strict';

/**
 *  slajder controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::slajder.slajder');
