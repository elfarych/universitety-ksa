'use strict';

/**
 * slajder service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::slajder.slajder');
