'use strict';

/**
 * slajder router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::slajder.slajder');
