'use strict';

/**
 * razdel service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::razdel.razdel');
