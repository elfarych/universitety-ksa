'use strict';

/**
 *  razdel controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::razdel.razdel');
