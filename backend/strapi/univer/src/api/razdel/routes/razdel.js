'use strict';

/**
 * razdel router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::razdel.razdel');
