from rest_framework import generics

from .serializers import ServiceListSerializer, ServiceSerializer, OrderSerializer
from .models import Service, Order


class ServiceListView(generics.ListAPIView):
    queryset = Service.objects.filter(public=True)
    serializer_class = ServiceListSerializer


class ServiceView(generics.RetrieveAPIView):
    queryset = Service.objects.filter(public=True)
    serializer_class = ServiceSerializer
    lookup_field = 'slug'


class CreateOrderView(generics.CreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
