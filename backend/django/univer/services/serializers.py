from rest_framework import serializers

from .models import Service, SubService, Order


class ServiceListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Service
        fields = ('title', 'slug', 'id', 'image')


class SubServiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubService
        fields = '__all__'


class ServiceSerializer(serializers.ModelSerializer):
    sub_services = SubServiceSerializer(many=True, read_only=True)

    class Meta:
        model = Service
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = '__all__'
