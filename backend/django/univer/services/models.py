from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField


class Service(models.Model):
    title = models.CharField('Название услуги', max_length=100)
    price = models.IntegerField('Цена')
    description = models.TextField('Краткое описание', max_length=200)
    content = RichTextUploadingField('Полное описание')
    image = ThumbnailerImageField('Фоновое фото', upload_to='logo/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})
    public = models.BooleanField('Опубликовать', default=True)
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True)
    slug = models.SlugField(unique=True)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'
        ordering = ('order',)


class SubService(models.Model):
    service = models.ForeignKey(Service, on_delete=models.SET_NULL, null=True, blank=True,
                                related_name='sub_services', verbose_name='Услуга')
    title = models.CharField('Название дополнительной услуги', max_length=100)
    price = models.IntegerField('Цена')
    description = models.TextField('Краткое описание', max_length=200)
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Дополнительная услуга'
        verbose_name_plural = 'Дополнительные услуги'
        ordering = ('order',)


class Order(models.Model):
    service = models.ForeignKey(Service, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Услуга',
                                related_name='orders')
    sub_service = models.ForeignKey(SubService, on_delete=models.SET_NULL, null=True, blank=True,
                                    verbose_name='Вариант', related_name='orders')
    name = models.CharField('Имя', max_length=255)
    email = models.CharField('Email', max_length=255)
    phone = models.CharField('Телефон', max_length=255)
    message = models.TextField('Сообщение', null=True, blank=True)
    completed = models.BooleanField('Обработано', default=False)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'
        ordering = ('-date',)
