from django.urls import path
from . import views

urlpatterns = [
    path('service_list/', views.ServiceListView.as_view()),
    path('detail/<slug:slug>/', views.ServiceView.as_view()),
    path('create_order/', views.CreateOrderView.as_view())
]
