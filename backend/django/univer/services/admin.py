from django.contrib import admin
from django.utils.safestring import mark_safe

from . import models


class SubServiceInline(admin.TabularInline):
    model = models.SubService
    extra = 0


@admin.register(models.Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'price', 'public', 'order', 'date', 'update')
    list_editable = ('order', 'public')
    list_filter = ('public', 'date', 'update')
    search_fields = ('title',)
    inlines = [SubServiceInline]
    prepopulated_fields = {'slug': ('title',)}
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.image:
            return mark_safe(
                f'<img src={obj.image.url} style="height: 50px; width: 70px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


@admin.register(models.SubService)
class SubServiceAdmin(admin.ModelAdmin):
    list_display = ('title', 'service', 'price', 'order', 'date', 'update')
    list_editable = ('order',)
    search_fields = ('title',)
    list_filter = ('service', 'date', 'update')
    save_as = True
    save_on_top = True


@admin.register(models.Order)
class SubServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'service', 'sub_service', 'completed', 'date', 'update')
    list_editable = ('completed',)
    search_fields = ('name',)
    list_filter = ('completed', 'service', 'date', 'update')
    save_as = True
    save_on_top = True
