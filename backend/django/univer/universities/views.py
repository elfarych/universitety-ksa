from rest_framework import generics

from .serializers import UniversityListSerializer, UniversitySerializer, UniversityChapterSerializer
from .models import University, UniversityChapter


class UniversityListView(generics.ListAPIView):
    queryset = University.objects.filter(public=True)
    serializer_class = UniversityListSerializer


class UniversityView(generics.RetrieveAPIView):
    queryset = University.objects.filter(public=True)
    serializer_class = UniversitySerializer
    lookup_field = 'slug'


class ChapterView(generics.RetrieveAPIView):
    queryset = UniversityChapter.objects.filter(public=True)
    serializer_class = UniversityChapterSerializer
    lookup_field = 'slug'
    