from django.urls import path
from . import views

urlpatterns = [
    path('universities_list/', views.UniversityListView.as_view()),
    path('university/<slug:slug>/', views.UniversityView.as_view()),
    path('chapter/<slug:slug>/', views.ChapterView.as_view()),
]