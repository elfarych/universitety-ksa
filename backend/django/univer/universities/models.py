from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField


class University(models.Model):
    title = models.CharField('Название университета', max_length=100)
    description = models.TextField('Краткое описание', max_length=200)
    content = RichTextUploadingField('Полное описание')
    image = ThumbnailerImageField('Фоновое фото', upload_to='logo/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})
    public = models.BooleanField('Опубликовать', default=True)
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True)
    slug = models.SlugField(unique=True)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Университет'
        verbose_name_plural = 'Университеты'
        ordering = ('order',)


class UniversityChapter(models.Model):
    university = models.ForeignKey(University, on_delete=models.SET_NULL, null=True, blank=True,
                                   related_name='chapters', verbose_name='Университет')
    title = models.CharField('Название раздела', max_length=100)
    description = models.TextField('Краткое описание', max_length=200)
    content = RichTextUploadingField('Полное описание')
    image = ThumbnailerImageField('Фоновое фото', upload_to='university/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True)
    public = models.BooleanField('Опубликовать', default=True)
    slug = models.SlugField(unique=True)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Раздел'
        verbose_name_plural = 'Разделы'
        ordering = ('order',)


class UniversityGallery(models.Model):
    chapter = models.ForeignKey(UniversityChapter, on_delete=models.SET_NULL, null=True, blank=True,
                                related_name='images', verbose_name='Раздел')
    image = ThumbnailerImageField('Фото', upload_to='university/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    def __str__(self):
        return f'{self.id}'

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фотогалерея'
        ordering = ('order',)


class ChapterVideo(models.Model):
    chapter = models.ForeignKey(UniversityChapter, on_delete=models.SET_NULL, null=True, blank=True,
                                related_name='videos', verbose_name='Раздел')
    link = models.CharField('Ссылка на видео', null=True, blank=True, max_length=255)
    title = models.CharField('Заголовок видео', null=True, blank=True, max_length=255)
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    def __str__(self):
        return f'{self.id}'

    class Meta:
        verbose_name = 'Видео'
        verbose_name_plural = 'Видеогалерея'
        ordering = ('order',)
