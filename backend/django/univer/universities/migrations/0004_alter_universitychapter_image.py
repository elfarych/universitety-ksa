# Generated by Django 3.2.5 on 2022-01-23 18:18

from django.db import migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('universities', '0003_chaptervideo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='universitychapter',
            name='image',
            field=easy_thumbnails.fields.ThumbnailerImageField(upload_to='university/', verbose_name='Фоновое фото'),
        ),
    ]
