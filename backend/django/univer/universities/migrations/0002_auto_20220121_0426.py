# Generated by Django 3.2.5 on 2022-01-20 22:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('universities', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='universitychapter',
            options={'ordering': ('order',), 'verbose_name': 'Раздел', 'verbose_name_plural': 'Разделы'},
        ),
        migrations.AddField(
            model_name='university',
            name='public',
            field=models.BooleanField(default=True, verbose_name='Опубликовать'),
        ),
    ]
