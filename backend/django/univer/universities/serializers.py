from rest_framework import serializers

from .models import University, UniversityChapter, UniversityGallery, ChapterVideo


class UniversityChapterListSerializer(serializers.ModelSerializer):

    class Meta:
        model = UniversityChapter
        fields = ('id', 'title', 'slug')


class UniversityListSerializer(serializers.ModelSerializer):
    chapters = UniversityChapterListSerializer(many=True, read_only=True)

    class Meta:
        model = University
        fields = ('id', 'title', 'slug', 'chapters', 'image', 'description')


class UniversitySerializer(serializers.ModelSerializer):
    chapters = UniversityChapterListSerializer(many=True, read_only=True)

    class Meta:
        model = University
        fields = '__all__'


class ChapterGallerySerializer(serializers.ModelSerializer):

    class Meta:
        model = UniversityGallery
        fields = '__all__'


class ChapterVideoSerializer(serializers.ModelSerializer):

    class Meta:
        model = ChapterVideo
        fields = '__all__'


class UniversityChapterSerializer(serializers.ModelSerializer):
    university = UniversityListSerializer(many=False, read_only=True)
    images = ChapterGallerySerializer(many=True, read_only=True)
    videos = ChapterVideoSerializer(many=True, read_only=True)

    class Meta:
        model = UniversityChapter
        fields = '__all__'


