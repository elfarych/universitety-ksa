from django.contrib import admin
from django.utils.safestring import mark_safe

from . import models


@admin.register(models.University)
class UniversityAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'public', 'order', 'date', 'update')
    list_editable = ('order', 'public')
    search_fields = ('title',)
    list_filter = ('public', 'date', 'update')
    prepopulated_fields = {'slug': ('title',)}
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.image:
            return mark_safe(
                f'<img src={obj.image.url} style="height: 50px; width: 70px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


class ChapterGalleryInline(admin.TabularInline):
    model = models.UniversityGallery
    extra = 0
    fields = ('get_image', 'image', 'order')
    readonly_fields = ('get_image',)

    def get_image(self, obj):
        if obj.image:
            return mark_safe(
                f'<img src={obj.image.url} style="height: 90px; width: 160px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


class ChapterVideoInline(admin.TabularInline):
    model = models.ChapterVideo
    extra = 0


@admin.register(models.UniversityChapter)
class UniversityChapterAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'university', 'public', 'order', 'date', 'update')
    list_editable = ('order', 'public')
    search_fields = ('title',)
    list_filter = ('public', 'university', 'date', 'update')
    prepopulated_fields = {'slug': ('title',)}
    inlines = [ChapterGalleryInline, ChapterVideoInline]
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.image:
            return mark_safe(
                f'<img src={obj.image.url} style="height: 50px; width: 70px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'
