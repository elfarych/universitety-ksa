from django.contrib import admin
from django.utils.safestring import mark_safe

from . import models


admin.site.register(models.AboutInfo)
admin.site.register(models.OurMission)
admin.site.register(models.Contacts)
admin.site.register(models.MainInfo)


@admin.register(models.Slider)
class SliderAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'title', 'order', 'date', 'update')
    list_editable = ('order',)
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.image:
            return mark_safe(
                f'<img src={obj.image.url} style="height: 50px; width: 70px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


@admin.register(models.Gallery)
class GalleryAdmin(admin.ModelAdmin):
    list_display = ('get_image', 'order', 'date', 'update')
    list_editable = ('order',)
    save_as = True
    save_on_top = True

    def get_image(self, obj):
        if obj.image:
            return mark_safe(
                f'<img src={obj.image.url} style="height: 50px; width: 70px; object-fit: cover; border-radius: 5px;">')

    get_image.short_description = 'Фото'


@admin.register(models.Consultation)
class ConsultationAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'update', 'completed', 'date', 'update')
    list_editable = ('completed',)
    list_filter = ('completed', 'date', 'update')
    save_as = True
    save_on_top = True


admin.site.site_title = 'Университеты КСА - администрирование'
admin.site.site_header = 'Университеты КСА'
