from rest_framework import generics

from .serializers import SlidesSerializer, ContactsSerializer, MainInfoSerializer, OurMissionSerializer, \
    AboutInfoSerializer, GallerySerializer, ConsultationSerializer
from .models import Slider, Contacts, MainInfo, AboutInfo, OurMission, Gallery, Consultation


class SliderView(generics.ListAPIView):
    queryset = Slider.objects.all()
    serializer_class = SlidesSerializer


class ContactsView(generics.ListAPIView):
    queryset = Contacts.objects.all()
    serializer_class = ContactsSerializer


class ImageGalleryView(generics.ListAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer


class MainInfoView(generics.ListAPIView):
    queryset = MainInfo.objects.all()
    serializer_class = MainInfoSerializer


class AboutView(generics.ListAPIView):
    queryset = AboutInfo.objects.all()
    serializer_class = AboutInfoSerializer


class OurMissionView(generics.ListAPIView):
    queryset = OurMission.objects.all()
    serializer_class = OurMissionSerializer


class ConsultationCreateView(generics.CreateAPIView):
    queryset = Consultation.objects.all()
    serializer_class = ConsultationSerializer
