from django.urls import path
from . import views

urlpatterns = [
    path('slides/', views.SliderView.as_view()),
    path('contacts/', views.ContactsView.as_view()),
    path('info/', views.MainInfoView.as_view()),
    path('about/', views.AboutView.as_view()),
    path('mission/', views.OurMissionView.as_view()),
    path('gallery/', views.ImageGalleryView.as_view()),
    path('create_consultation/', views.ConsultationCreateView.as_view())
]