# Generated by Django 3.2.5 on 2022-01-23 13:46

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0002_auto_20220121_0404'),
    ]

    operations = [
        migrations.AddField(
            model_name='contacts',
            name='contacts_text',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контакты текст'),
        ),
    ]
