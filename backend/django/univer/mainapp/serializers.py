from rest_framework import serializers
from .models import Slider, Contacts, MainInfo, AboutInfo, OurMission, Gallery, Consultation


class SlidesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slider
        fields = '__all__'


class ContactsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contacts
        fields = '__all__'


class MainInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MainInfo
        fields = '__all__'


class AboutInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutInfo
        fields = '__all__'


class OurMissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = OurMission
        fields = '__all__'


class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = '__all__'


class ConsultationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consultation
        fields = '__all__'
