from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField


class Slider(models.Model):
    title = models.CharField('Заголовок', max_length=100)
    sub_title = models.CharField('Подзаголовок', max_length=100)
    image = ThumbnailerImageField('Миниатюра', upload_to='univers/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайды'
        ordering = ('order',)


class MainInfo(models.Model):
    title = models.CharField('Название компании', max_length=100)
    text = models.TextField('Краткое описание', max_length=200)
    logo = ThumbnailerImageField('логотип', upload_to='logo/',
                                 resize_source={'size': (500, 500), 'crop': 'scale'})
    brand_color = models.CharField('Фирменный цвет', null=True, blank=True, max_length=20)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Настройки сайта'
        verbose_name_plural = 'Настройки сайта'


class Contacts(models.Model):
    title = models.CharField('Заголовок для страницы контактов', max_length=150)
    email = models.EmailField('Email', null=True, blank=True)
    whatsapp = models.CharField('Whatsapp', null=True, blank=True, max_length=20)
    phone = models.CharField('Телефон', null=True, blank=True, max_length=20)
    telegram = models.CharField('Telegram', null=True, blank=True, max_length=255)
    instagram = models.CharField('Instagram', null=True, blank=True, max_length=255)
    facebook = models.CharField('Facebook', null=True, blank=True, max_length=255)
    youtube = models.CharField('Youtube', null=True, blank=True, max_length=255)
    vk = models.CharField('VK', null=True, blank=True, max_length=255)
    contacts_text = RichTextUploadingField('Контакты текст', null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'


class AboutInfo(models.Model):
    title = models.CharField('Заголовк страницы', max_length=100)
    text = models.TextField('Краткое описание', max_length=200)
    content = RichTextUploadingField('Полное описание')
    image = ThumbnailerImageField('Фоновое фото', upload_to='logo/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Информация о компании'
        verbose_name_plural = 'Информация о компании'


class OurMission(models.Model):
    title = models.CharField('Заголовк страницы', max_length=100)
    text = models.TextField('Краткое описание', max_length=200)
    content = RichTextUploadingField('Полное описание')
    image = ThumbnailerImageField('Фоновое фото', upload_to='logo/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Нaша миссия'
        verbose_name_plural = 'Нaша миссия'


class Gallery(models.Model):
    image = ThumbnailerImageField('Фото', upload_to='gallery/',
                                  resize_source={'size': (1000, 1000), 'crop': 'scale'})
    order = models.PositiveSmallIntegerField('Порядковый номер', null=True, blank=True)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    def __str__(self):
        return f'{self.id}'

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фотогалерея'
        ordering = ('order',)


class Consultation(models.Model):
    name = models.CharField('Имя', max_length=255)
    email = models.CharField('Email', max_length=255)
    phone = models.CharField('Телефон', max_length=255)
    message = models.TextField('Сообщение', null=True, blank=True)
    completed = models.BooleanField('Обработано', default=False)
    date = models.DateTimeField('Создан', auto_now_add=True)
    update = models.DateTimeField('Обновлен', auto_now=True)

    class Meta:
        verbose_name = 'Консультация'
        verbose_name_plural = 'Консультации'
        ordering = ('-date',)

