import { colors } from 'quasar'

export default function setBrandColor (color) {
  return color ? colors.setBrand('primary', color) : null
}
