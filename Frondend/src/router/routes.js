
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/university',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: ':slug',
        name: 'university',
        component: () => import('pages/University')
      }
    ]
  },
  {
    path: '/chapter',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'chapter',
        path: ':slug',
        component: () => import('pages/UniversityCapter')
      }
    ]
  },
  {
    path: '/service',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'service',
        path: ':slug',
        component: () => import('pages/Service')
      }
    ]
  },
  {
    path: '/about',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'about',
        path: '',
        component: () => import('pages/About')
      }
    ]
  },
  {
    path: '/mission',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'mission',
        path: '',
        component: () => import('pages/OurMission')
      }
    ]
  },

  {
    path: '/contacts',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'contacts',
        path: '',
        component: () => import('pages/Contacts')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
