export function mutationSettings (state, data) {
  state.settings = data
}

export function mutationUniversities (state, data) {
  state.universities = data
}

export function mutationUniversityDetail (state, data) {
  state.university = data
}

export function mutationUniversityChapter (state, data) {
  state.chapter = data
}

export function mutationServices (state, data) {
  state.services = data
}

export function mutationContacts (state, data) {
  state.contacts = data
}

export function mutationServiceDetail (state, data) {
  state.service = data
}
