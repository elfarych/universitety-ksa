import axios from 'axios'
import server from 'src/config'
import errorHandler from 'src/utils/error-handler'
import setBrandColor from 'src/utils/set-brand-color'

export async function LoadMainSettings ({ commit }) {
  try {
    await axios.get(`${server.serverURI}/main/info/`)
      .then(res => {
        setBrandColor(res.data?.results?.[0].brand_color || '')
        commit('mutationSettings', res.data?.results?.[0] || {})
      })
  } catch (e) {
    errorHandler(e)
  }
}

export async function LoadUniversities ({ commit }) {
  try {
    await axios.get(`${server.serverURI}/universities/universities_list/`)
      .then(res => {
        return commit('mutationUniversities', res.data?.results || [])
      })
  } catch (e) {
    errorHandler(e)
  }
}

export async function LoadUniversity ({ commit }, slug) {
  try {
    await axios.get(`${server.serverURI}/universities/university/${slug}/`)
      .then(res => {
        return commit('mutationUniversityDetail', res.data)
      })
  } catch (e) {
    errorHandler(e)
  }
}

export async function LoadUniversityChapter ({ commit }, slug) {
  try {
    await axios.get(`${server.serverURI}/universities/chapter/${slug}/`)
      .then(res => {
        return commit('mutationUniversityChapter', res.data)
      })
  } catch (e) {
    errorHandler(e)
  }
}

export async function LoadServices ({ commit }) {
  try {
    await axios.get(`${server.serverURI}/service/service_list/`)
      .then(res => {
        commit('mutationServices', res.data?.results || [])
      })
  } catch (e) {
    errorHandler(e)
  }
}

export async function LoadServiceDetail ({ commit }, slug) {
  try {
    await axios.get(`${server.serverURI}/service/detail/${slug}/`)
      .then(res => {
        return commit('mutationServiceDetail', res.data)
      })
  } catch (e) {
    errorHandler(e)
  }
}

export async function LoadContacts ({ commit }) {
  try {
    await axios.get(`${server.serverURI}/main/contacts/`)
      .then(res => {
        commit('mutationContacts', res.data?.results?.[0] || [])
      })
  } catch (e) {
    errorHandler(e)
  }
}

export function init ({ dispatch }) {
  return Promise.all([
    dispatch('LoadMainSettings'),
    dispatch('LoadUniversities'),
    dispatch('LoadServices'),
    dispatch('LoadContacts')
  ]).then(() => {})
}
