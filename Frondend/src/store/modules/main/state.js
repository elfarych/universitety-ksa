export default function () {
  return {
    settings: {},
    universities: [],
    university: {},
    chapter: {},
    services: [],
    service: {},
    contacts: {}
  }
}
