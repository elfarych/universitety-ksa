import * as actions from './actions'
import state from './state'
import * as mutations from './mutations'

export default {
  state,
  actions,
  mutations,
  namespaced: true
}
